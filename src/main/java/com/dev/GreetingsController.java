package com.dev;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsController {

    @RequestMapping("/")
    public Greetings greeting(@RequestParam(value="name", defaultValue = "Book Shop") String name) {
        return new Greetings(name);
    }
}
