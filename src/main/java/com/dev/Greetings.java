package com.dev;

public class Greetings {

    private String name;

    public Greetings(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}