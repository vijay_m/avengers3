FROM openjdk:8-alpine
ADD /build/libs/bookshop-0.1.0.jar bookshop.jar
ENTRYPOINT ["java", "-jar", "/bookshop.jar"]